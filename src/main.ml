
let (>>) f g x =
    g (f x)

let main = function
    | _ :: args ->
        List.iter (Parse.model >> ignore) args
    | [] -> assert false

let () = Sys.argv |> Array.to_list |> main
