%token EOF
%token <string> ID CON
%token <int> NAT
%token LPAR RPAR
%token COMMA DOT COLON BAR LAMBDA UNDERSCORE
%token AND OR NOT IMP ALL EX TRUE FALSE
%token CASE OF END IS BEGIN MODEL THEORY AS
%token THEOREM VALUE RELATION DEFINE
%token INDUCTIVE COINDUCTIVE PROOF QED ADMIT

%{
open Parse_tree

let split_last = 
    let rec loop acc = function
        | [] -> assert false
        | [x] -> List.rev acc, x
        | x :: xs -> loop (x :: acc) xs
    in
    loop []
%}

%start <Parse_tree.Toplevel.t list> model_file
%start <Parse_tree.Signature.t list> theory_file
%%

model_file:
    | ts=toplevel*; EOF
        {ts}

theory_file:
    | ss=signature*; EOF
        {ss}

toplevel:
    | t=toplevel_
        {Info.make $loc t}

toplevel_:
    | THEOREM; name=CON; IS; goal=prop; PROOF; proof=proof; QED
        {Toplevel.Theorem {name; goal; proof}}
    | DEFINE; fixpoint=define_type; name=CON; args=ID*; AS; body=prop; END
        {Toplevel.Definition {fixpoint; name; args; body}}
    | VALUE; x=ID; IS; t=term; END
        {Toplevel.Value (x, t)}
    | MODEL; name=CON; args=module_arg*;
            theory=theory_annotation; BEGIN; body=model; END
        {Toplevel.Model {name; args; theory; body}}
    | THEORY; name=CON; args=module_arg*; IS; theory=theory; END
        {Toplevel.Theory {name; args; theory}}

model:
    | m=model_
        {Info.make $loc m}

model_:
    | ds=toplevel*
        {Model.Literal ds}
    | e=model_expression 
        {Model.Named e}

model_expression:
    | m=model_expression_
        {Info.make $loc m}

model_expression_:
    | head=model_expression_atom; args=model_expression_atom*
        {Model_expression.App (head, args)}

model_expression_atom:
    | m=model_expression_atom_
        {Info.make $loc m}

model_expression_atom_:
    | LPAR; m=model_expression_; RPAR
        {m}
    | c=qualified_con
        {Model_expression.Var c}
    
theory_annotation:
    | 
        {None}
    | IS; t=theory_
        {Some (Info.make $loc t)}

theory:
    | t=theory_
        {Info.make $loc t}

theory_:
    | e=theory_expression
        {Theory.Expression e}
    | ss=signature*
        {Theory.Definition ss}

theory_expression:
    | t=theory_expression_
        {Info.make $loc t}

theory_expression_:
    | head=theory_expression_atom; args=theory_expression_atom*
        {Theory_expression.App (head, args)}

theory_expression_atom:
    | t=theory_expression_atom_
        {Info.make $loc t}

theory_expression_atom_:
    | c=qualified_con
        {Theory_expression.Atom c}
    | LPAR; t=theory_expression_; RPAR
        {t}

signature:
    | s=signature_
        {Info.make $loc s}

signature_:
    | VALUE; i=ID
        {Signature.Value i}
    | RELATION; name=CON; arity=NAT
        {Signature.Relation {name; arity}}
    | THEOREM; c=CON; IS; p=prop;
        {Signature.Theorem (c, p)}
    | MODEL; name=CON; arguments=module_arg*; IS; theory=theory; END
        {Signature.Model {name; arguments; theory}}

module_arg:
    | LPAR; c=CON; COLON; t=theory_expression RPAR
        {(c, t)}

qualified_con:
    | qc=qualified_con_
        {Info.make $loc qc}

qualified_con_:
    | full=separated_nonempty_list(DOT, CON)
        {let path, elem = split_last full in
         Qualified_name.{path; elem}}

path:
    | p=terminated(CON, DOT)*
        {p}
    
define_type:
    | 
        {Toplevel.Any}
    | INDUCTIVE
        {Toplevel.Least}
    | COINDUCTIVE
        {Toplevel.Greatest}

prop:
    | p=prop_
        {Info.make $loc p}

prop_:
    | ALL; vars=ID+; restriction=member_annotation?; COMMA; body=prop
        {Prop.All {vars; restriction; body}}
    | EX; vars=ID+; restriction=member_annotation?; COMMA; body=prop
        {Prop.Ex {vars; restriction; body}}
    | CASE; values=separated_nonempty_list(COMMA, term); OF;
            BAR?; clauses=separated_list(BAR, prop_case_clause); END
        {Prop.Case {values; clauses}}
    | p=prop_imp_
        {p}

prop_imp:
    | p=prop_imp_
        {Info.make $loc p}

prop_imp_:
    | p0=prop_disj; IMP; p1=prop_imp
        {Prop.Imp (p0, p1)}
    | p=prop_disj_
        {p}

prop_disj:
    | p=prop_disj_
        {Info.make $loc p}

prop_disj_:
    | p0=prop_conj; OR; p1=prop_disj
        {Prop.Or (p0, p1)}
    | p=prop_conj_
        {p}

prop_conj:
    | p=prop_conj_
        {Info.make $loc p}

prop_conj_:
    | p0=prop_atom; AND; p1=prop_conj
        {Prop.And (p0, p1)}
    | p=prop_atom_
        {p}

prop_atom:
    | p=prop_atom_
        {Info.make $loc p}

prop_atom_:
    | TRUE
        {Prop.True}
    | FALSE
        {Prop.False}
    | r=qualified_con; ts=term_atom*
        {Prop.Rel (r, ts)}
    | NOT; p=prop_atom
        {Prop.Not p}
    | LPAR; p=prop_; RPAR
        {p}

member_annotation:
    | COLON; c=qualified_con; ts=term_atom*
        {c, ts}

prop_case_clause:
    | ps=separated_nonempty_list(COMMA, pattern); COLON; p=prop
        {(ps, p)}

term:
    | t=term_
        {Info.make $loc t}

term_:
    | LAMBDA; xs=ID+; COMMA t=term
        {Term.Lam (xs, t)}
    | CASE; values=separated_nonempty_list(COMMA, term); OF
            BAR?; clauses=separated_list(BAR, term_case_clause); END
        {Term.Case {values; clauses}}
    | t=term_app
        {t.e}

term_case_clause:
    | ps=separated_nonempty_list(COMMA, pattern); COLON; t=term
        {(ps, t)}

term_app:
    | ts=term_atom+
        { match ts with [] -> assert false | t :: _ -> t }

term_atom:
    | t=term_atom_
        {Info.make $loc t}

term_atom_:
    | path=path; elem=ID
        {Term.Var (Info.make $loc Qualified_name.{path; elem})}
    | c=CON
        {Term.Con c}
    | LPAR; t=term_; RPAR
        {t}

pattern:
    | p=pattern_
        {Info.make $loc p}

pattern_:
    | c=CON; ps=pattern_atom*
        {Pattern.Con (c, ps)}
    | p=pattern_atom_
        {p}

pattern_atom:
    | p=pattern_atom_
        {Info.make $loc p}

pattern_atom_:
    | UNDERSCORE
        {Pattern.Ignored}
    | i=ID
        {Pattern.Var i}
    | LPAR; p=pattern_; RPAR
        {p}

proof:
    | ADMIT
        {Info.make $loc Proof.Admit}

