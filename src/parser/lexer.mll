{
    open Lexing 
    open Parser
}

let white = [' ' '\t']
let newline = "\r" | "\n" | "\r\n"
let comment = "--" [^ '\n' '\r']*

let lower_letter = ['a'-'z']
let upper_letter = ['A'-'Z']
let letter = upper_letter | lower_letter
let digit = ['0'-'9']
let punct = "_" | "-"

let id_char = letter | digit | punct
let id = lower_letter id_char*
let con = upper_letter id_char*


rule read = parse
    | white+ {read lexbuf}
    | newline {new_line lexbuf; read lexbuf}
    | comment {read lexbuf}
    | eof {EOF}

    | "(" {LPAR}
    | ")" {RPAR}

    | "\\" {LAMBDA}
    | "." {DOT}
    | "," {COMMA}
    | ":" {COLON}
    | "|" {BAR}
    | "_" {UNDERSCORE}

    | "case" {CASE}
    | "of" {OF}
    | "end" {END}

    | "True" {TRUE}
    | "False" {FALSE}
    | "not" {NOT}
    | "and" {AND}
    | "or" {OR}
    | "=>" {IMP}
    | "all" {ALL}
    | "ex" {EX}

    | "theory" {THEORY}
    | "model" {MODEL}
    | "value" {VALUE}
    | "begin" {BEGIN}
    | "theorem" {THEOREM}
    | "define" {DEFINE}
    | "inductive" {INDUCTIVE}
    | "coinductive" {COINDUCTIVE}
    | "relation" {RELATION}
    | "admit" {ADMIT}
    | "proof" {PROOF}
    | "qed" {QED}
    | "is" {IS}
    | "as" {AS}

    | id as n {ID n}
    | con as c {CON c}
    | digit+ as n {NAT (int_of_string n)}

