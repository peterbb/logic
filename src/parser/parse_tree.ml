
module Info = struct
    type 'a t =
        { e : 'a
        ; start_line : int
        ; start_column : int
        ; end_line : int
        ; end_column : int
        }

    let make loc e =
        let open Lexing in
        let startpos = fst loc in
        let endpos = snd loc in
        let col {pos_cnum; pos_bol; _} = pos_cnum - pos_bol in
        { e
        ; start_line = startpos.pos_lnum
        ; end_line = endpos.pos_lnum
        ; start_column = col startpos
        ; end_column = col endpos
        }
end

module Qualified_name = struct
    type t =
        t' Info.t
    and t' =
        { path : string list
        ; elem : string
        }
end

module Pattern = struct
    type t =
        t' Info.t
    and t' =
        | Ignored
        | Var of string
        | Con of string * t list
end

module Term = struct
    type t =
        t' Info.t
    and t' =
        | Var of Qualified_name.t
        | App of t * t
        | Lam of string list * t
        | Con of string
        | Case of
            { values : t list
            ; clauses : (Pattern.t list * t) list
            }
end

module Prop = struct
    type t =
        t' Info.t
    and t' =
        | True
        | False
        | Rel of Qualified_name.t * Term.t list
        | Not of t
        | And of t * t
        | Or of t * t
        | Imp of t * t
        | All of
            { vars : string list
            ; restriction : (Qualified_name.t * Term.t list) option
            ; body : t
            }
        | Ex of
            { vars : string list
            ; restriction : (Qualified_name.t * Term.t list) option
            ; body : t
            }
        | Case of
            { values : Term.t list
            ; clauses : (Pattern.t list * t) list
            }
end

module Proof = struct
    type t =
        t' Info.t
    and t' =
        | Admit
end

module Theory_expression = struct
    type t =
        t' Info.t
    and t' =
        | Atom of Qualified_name.t
        | App of t * t list
end

module rec Theory : sig
    type t =
        t' Info.t
    and t' =
        | Expression of Theory_expression.t
        | Definition of Signature.t list
end = Theory

and Signature : sig
    type t =
        t' Info.t
    and t' =
        | Value of string
        | Relation of
            { name : string
            ; arity : int
            }
        | Theorem of string * Prop.t
        | Model of 
            { name : string
            ; arguments : (string * Theory_expression.t) list
            ; theory : Theory.t
            }
end = Signature

module Model_expression = struct
    type t =
        t' Info.t
    and t' =
        | Var of Qualified_name.t
        | App of t * t list
end

module rec Model : sig
    type t =
        t' Info.t
    and t' =
        | Named of Model_expression.t
        | Literal of Toplevel.t list
end  = Model

and Toplevel : sig 
    type fixpoint = Least | Greatest | Any
        
    type t =
        t' Info.t
    and t' =
        | Value of string * Term.t
        | Definition of
            { fixpoint : fixpoint
            ; name : string
            ; args : string list
            ; body : Prop.t
            }
        | Theorem of
            { name : string
            ; goal : Prop.t
            ; proof : Proof.t
            }
        | Theory of
            { name : string
            ; args : (string * Theory_expression.t) list
            ; theory: Theory.t
            }
        | Model of
            { name : string
            ; args : (string * Theory_expression.t) list
            ; theory : Theory.t option
            ; body : Model.t
            }
end = Toplevel

