
let with_lexbuf_from_file ~filename body =
    let ch = open_in filename in
    let lexbuf = Lexing.from_channel ch in
    try
        let result = body lexbuf in
        close_in ch;
        result
    with
    | e -> (close_in ch; raise e)

let model filename =
    with_lexbuf_from_file ~filename
    begin fun lexbuf ->
        Parser.model_file Lexer.read lexbuf
    end
    
