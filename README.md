# logic

This is intended to be an implementation of a logic.
The plan is to have a base of first-order intuitionstic logic with
 induction/coinduction over untyped lambda calculus with constructors and
pattern matching.
On top of this, a module system will allow limited higher-order reasoning
and reuse.

## Terms

We have core lambda calculus with single-argument anonymous functions, application and variables. For ease of use, we have onstructors of arbitrary arity, non-linear pattern matching and general recursion on top of this.

## Core propositions

We have the traditional connectives of first-order logic (without equality), conjunction, disjunction, implication and negation, and the universal and existential quanifiers. Furthermore, we allow inductive and coinductive definitions of relations. Finally, we have a proposition for linear pattern matching of terms.

## Core proofs

The rules will be a sequent calculus similar to logic G. Because our term language does not have normal forms, there must be some rules regarding computation.

## Meta-logical propositions

Through a ML-like module system, it will essentially be possible to define prenex-form second-order relations and proofs about these relations.